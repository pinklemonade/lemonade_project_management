from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from project_management.models import *


# Register your models here.

@admin.register(ProjectTask)
class ProjectTaskAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = (
        'id', 'project_name', 'phase', 'details', 'resource', 'planned_completion_date', 'planned_approval_date',
        'revised_date', 'revised_completion_date', 'revised_approval_date', 'status')
    list_filter = ('project_name', 'phase', 'details', 'resource', 'status')
