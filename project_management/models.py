from django.db import models
from lemonade_project_management.settings import DATE_INPUT_FORMATS


# Create your models here.
class ProjectTask(models.Model):
    PROJECT_STATUS = (
        ('Status', 'Status'),
        ('Completed', 'Completed'),
        ('Delayed', 'Delayed'),
        ('Onhold', 'Onhold'),
        ('WIP', 'WIP'),
        ('Waiting for comment', 'Waiting for comment'),
        ('Approved', 'Approved')
    )

    project_name = models.CharField(max_length=450, default='', null=True, blank=True)
    phase = models.CharField(max_length=250, null=True, blank=True)
    details = models.CharField(max_length=450, null=True, blank=True)
    resource = models.CharField(max_length=450, null=True, blank=True)
    planned_completion_date = models.DateField(auto_now=False, null=True, blank=True)
    planned_approval_date = models.DateField(auto_now=False, null=True, blank=True)
    revised_date = models.DateField(auto_now=False, null=True, blank=True)
    revised_completion_date = models.DateField(auto_now=False, null=True, blank=True)
    revised_approval_date = models.DateField(auto_now=False, null=True, blank=True)
    status = models.CharField(max_length=450, choices=PROJECT_STATUS, default='Status')
    comments = models.TextField(null=True, blank=True)
    links = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.project_name

    class Meta:
        ordering = ['project_name', 'details']
